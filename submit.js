let submittedForm = document.querySelector('#submittedForm');
let form = document.querySelector('#myForm');

form.addEventListener('submit', function (event) {
    event.preventDefault();

    const xhr = new XMLHttpRequest();
    const url = '/posturl/index.json';

    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    let data = new FormData(form);
    data.append('newField', 'customValue');
    data.delete('hiddenInput');

    xhr.onreadystatechange = function () {
        if (xhr.status !== 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            console.log(xhr.responseText);
            submittedForm.classList.toggle('active');
        }
    }

    xhr.send(data);
});
